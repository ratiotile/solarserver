from django.db import models
from datalogger.models import Sensor, DataType

# Create your models here.
class DataChart(models.Model):
    """ Represents a chart of data from selected sensors.
    """
    name = models.CharField(max_length=63)
    sensors = models.ManyToManyField(Sensor)