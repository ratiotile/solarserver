from django.contrib import admin

# Register your models here.
from dataviewer import models

@admin.register(models.DataChart)
class DataChartAdmin(admin.ModelAdmin):
    pass