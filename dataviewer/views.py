from django.http import JsonResponse
from django.shortcuts import render, get_object_or_404
from django.views.generic import View
from dataviewer.models import DataChart
from datalogger.models import DataValue
from django.core import serializers
from django.core.serializers.json import DjangoJSONEncoder
from django.core.serializers.python import Serializer
import json
from copy import copy
# Create your views here.

class FlatPySerializer(Serializer):
    """ serialize to python, removing 'id', 'model', and flattening 'fields'
    """
    def __init__(self):
        super().__init__()
        self._include_pk = False

    def serialize(self, queryset, **options):
        if 'pk' in options:
            self._include_pk = options['pk']
        return super().serialize(queryset, **options)

    def end_object(self, obj):
        if self._include_pk is True:
            cur = copy(self._current)
            cur['pk'] = obj._get_pk_val()
            self.objects.append(cur)
        else:
            self.objects.append(self._current)

class SetSerializer(FlatPySerializer):
    """ serialize to dict where key=pk
    """
    def serialize(self, queryset, **options):
        options['pk'] = True
        slist = super().serialize(queryset, **options)
        return {x['pk']: x for x in slist}

class ChartView(View):
    def get(self, request, target_chart):
        datachart = get_object_or_404(DataChart, pk=target_chart)
        sensordata = self._get_sensor_data(datachart)

        serialized = json.dumps(sensordata, cls=DjangoJSONEncoder)

        return self._histogram(request, {'sensor_data':serialized})


    def _histogram(self, request, context):
        return render(request, template_name='dataviewer/histogram.html',
                      context=context)

    def _get_sensor_data(self, datachart):
        sensors = datachart.sensors.all()
        sensor_pks = [s.pk for s in sensors]
        type_pks = [s.type for s in sensors]
        loc_pks = [s.location for s in sensors]
        dataset = DataValue.objects.filter(sensor__pk__in=sensor_pks)
        #typeset = DataType.objects.filter()
        flatSerializer = FlatPySerializer()
        setSerializer = SetSerializer()
        data = {
            "sensors": setSerializer.serialize(sensors),
            "data": flatSerializer.serialize(dataset),
            "types": setSerializer.serialize(type_pks),
            "locations": setSerializer.serialize(loc_pks),
        }
        return data