# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='DataType',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('name', models.CharField(max_length=63)),
                ('units', models.CharField(max_length=63)),
            ],
        ),
        migrations.CreateModel(
            name='DataValue',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('value', models.FloatField()),
                ('time', models.DateTimeField(auto_now=True)),
            ],
        ),
        migrations.CreateModel(
            name='Location',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('name', models.CharField(unique=True, max_length=63)),
            ],
        ),
        migrations.CreateModel(
            name='Sensor',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('name', models.CharField(unique=True, max_length=63)),
                ('location', models.ForeignKey(to='datalogger.Location')),
            ],
        ),
        migrations.AddField(
            model_name='datavalue',
            name='sensor',
            field=models.ForeignKey(to='datalogger.Sensor'),
        ),
        migrations.AddField(
            model_name='datavalue',
            name='type',
            field=models.ForeignKey(to='datalogger.DataType'),
        ),
        migrations.AlterUniqueTogether(
            name='datatype',
            unique_together=set([('name', 'units')]),
        ),
    ]
