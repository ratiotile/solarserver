# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('datalogger', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='datavalue',
            name='type',
        ),
        migrations.AddField(
            model_name='sensor',
            name='type',
            field=models.ForeignKey(default=1, to='datalogger.DataType'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='datavalue',
            name='time',
            field=models.DateTimeField(auto_now_add=True),
        ),
    ]
