from django.db import models

# Create your models here.
class DataType(models.Model):
    name = models.CharField(max_length=63)
    units = models.CharField(max_length=63)

    class Meta:
        unique_together = ('name', 'units')

    def __str__(self):
        return "{}({})".format(self.name, self.units)


class Location(models.Model):
    name = models.CharField(max_length=63,
                            unique=True)

    def __str__(self):
        return self.name


class Sensor(models.Model):
    name = models.CharField(max_length=63,
                            unique=True)
    location = models.ForeignKey(to=Location)
    type = models.ForeignKey(to=DataType)

    def __str__(self):
        return self.name


class DataValue(models.Model):
    sensor = models.ForeignKey(to=Sensor)
    value = models.FloatField()
    time = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "{} {}".format(self.value, str(self.type.units))