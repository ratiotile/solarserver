from django.contrib import admin

# Register your models here.
from datalogger import models

@admin.register(models.DataType)
class DataTypeAdmin(admin.ModelAdmin):
    pass

@admin.register(models.Sensor)
class SensorAdmin(admin.ModelAdmin):
    pass

@admin.register(models.Location)
class LocationAdmin(admin.ModelAdmin):
    pass

@admin.register(models.DataValue)
class DataValueAdmin(admin.ModelAdmin):
    list_filter = ('sensor', 'value', 'time')
    list_display = ('__str__', 'sensor', 'value', 'time')
